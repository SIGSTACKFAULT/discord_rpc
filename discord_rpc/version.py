from collections import namedtuple

version_info = namedtuple("discord_rpc", ["major", "minor", "patch"])(0,1,0)

version = "{}.{}.{}".format(*version_info)
