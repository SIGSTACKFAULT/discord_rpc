import os
from collections import namedtuple
import json
import struct
import logging
import socket
import uuid

from .version import *

if(__name__ == "__main__"):
	print(version)


OP = namedtuple("OP", ["HANDSHAKE","FRAME","CLOSE","PING","PONG"])(0,1,2,3,4)
struct_format = "<II"

logger = logging.getLogger(__name__)



class rpc:
	def __init__(self, cid, mask_cid=True):
		logger.info("discord_rpc %s", version)
		
		self.cid = cid
		self.sock_format = self._get_sock()
		self.nonce = str(uuid.uuid4())
		
		if(mask_cid):
			self.masked_cid = cid[-4:].rjust(len(cid), "*")
		else:
			self.masked_cid = cid
		
		self._connect()
		self._handshake()
		logger.info("Connected via id %s", self.masked_cid)

	def __call__(self, a):
		self._send(
			{
				"cmd" : "SET_ACTIVITY",
				"args" : {
					"pid" : os.getpid(),
					"activity": a
				},
				"nonce" : self.nonce
			},
			op=OP.FRAME
		)

################################################# IO FUNCTIONS


	def _send(self, d, op=OP.FRAME):
		data = json.dumps(d,separators=(',',':')).encode()
		header = struct.pack(struct_format, op, len(data))
		self._sock.sendall(header + data)

	
	def _recv(self, size=-1):
		if(size == -1):
			op, size = struct.unpack(struct_format, self._recv(8))
			return op, json.loads(self._sock.recv(size))
		else:
			return self._sock.recv(size)


################################################# CONNECTION & HANDSHAKE


	def _get_sock(self):
		# TODO: Be less lazy
		return os.path.join(os.environ.get("XDG_RUNTIME_DIR"), "discord-ipc-{n}")
		
	def _connect(self):
		self._sock = socket.socket(socket.AF_UNIX)
		for i in range(10):
			path = self.sock_format.format(n=i)
			if(os.path.exists(path)):
				self._sock.connect(path)
				logger.debug("Connected on %s", path)
				return
		raise Exception("No socket found")
		
	
	def _handshake(self):
		hs = {"v":1,"client_id":self.cid}
		self._send(hs, op=OP.HANDSHAKE)
		op, d = self._recv()
		if(op == 1 and d["cmd"] == "DISPATCH"):
			return
		else:
			self._sock.close()
			raise RuntimeError("Invalid response")
