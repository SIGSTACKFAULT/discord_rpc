from setuptools import setup

import discord_rpc.version

setup(
	name="discord_rpc",
	version = discord_rpc.version,
	description = "A Discord rich presence client",
	url="",
	author_email="blacksilverck35@gmail.com",
	license="LGPL",
	packages=["discord_rpc"],
	zip_safe=False
)
